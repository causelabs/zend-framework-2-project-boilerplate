<?php

/**
 * PHP Built-in Web Server wrapper for ZF apps
 * Uses the PHP 5.4 built-in web server to serve apps in lieu of Apache and .htaccess
 * and mod_rewrite
 *
 * http://arr.gr/blog/2012/08/serving-zf-apps-with-the-php-54-built-in-web-server/
 */

//Let the site know that this is the development environment
@putenv("APP_ENV=LocalDev");

$reqUri = $_SERVER['REQUEST_URI'];
$_SERVER['SERVER_NAME'] = 'balancingact.localhost';
$_SERVER['SITE_HOST'] = 'balancingact.localhost';
if (strpos($reqUri, '?') !== false) {
    $reqUri = substr($reqUri, 0, strpos($reqUri, '?'));
}

//custom routs for server-specific routes
if (substr_count($reqUri, '/css/') > 0) {
    $target = realpath(__DIR__ . '/../.tmp' . $reqUri);
    if ($target && !empty($target) && file_exists($target)) {
        header("Content-type: text/css");
        readfile($target);
        exit;
    }
} elseif (substr_count($reqUri, 'bower_components') > 0) {
    $target = realpath(__DIR__ . '/..' . $reqUri);
    if ($target) {
        $extension = strtolower(array_pop(explode(".", $target)));
        if ($extension == 'css') {
            header("Content-type: text/css");
        } elseif ($extension == 'js') {
            header("Content-type: application/javascript");
        } elseif ($extension == 'woff') {
            header("Content-type: application/x-font-woff");
        } elseif ($extension == 'ttf') {
            header("Content-type: application/x-font-ttf");
        } elseif ($extension == 'png') {
            header("Content-type: image/png");
        }
        readfile($target);
        exit;
    }
} elseif (substr_count($reqUri, '.json') > 0) {
    $target = realpath(__DIR__ . $reqUri);
    if ($target) {
        header("Content-type: text/application-json");
        readfile($target);
        exit;
    }
}

$target = realpath(__DIR__ . $reqUri);

if ($target && is_file($target)) {
    // Tell PHP to directly serve the requested file
    return false;
}

// Load the ZF app front controller script
require __DIR__ . '/index.php';
