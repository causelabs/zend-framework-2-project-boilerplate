Zend Framework 2 Boilerplate Application
========================================

Introduction
------------
This boilerplate is designed to help you get a Zend Framework 2 site up and going quickly. But instead of containing just the bare framework, it includes a lot of additional features to make this the backbone of a full-featured production site.


What's Included?
----------------
* Bower - Package manager for JS libraries.
* Grunt build system - Fully-configured build script that provides the following features:
    * SASS compilation
    * JS/CSS minification
    * Image optimization
    * Static file versioning with automatic template reference rewrite
    * Auto CSS prefixer - Have `transform` in your CSS? It will automatically add `--webkit-transform`
    * Copies production files into the /dist folder, so that you can just copy the contents of that folder to the server
* PHP development server - Just type `grunt serve` into the command line, and you're up and going.
* jQuery - You know you're going to use it.
* html5shiv and respond.js - for effortless IE support and graceful degradation.
* Doctrine - Just enable it in /config/application.config.php
* Zend Framework 2
* Zend Developer Tools - Already configured to run only in the dev environment.


Installation
------------

1) Install Dependencies

* Node - [Install it here.](https://nodejs.org/)
* Bower - `npm install -g bower`
* Grunt - `npm install -g grunt-cli`
* PHP 5.4 or later

2) Check out this repository

3) Install the packages
```
php composer.phar selfupdate
php composer.phar install
npm install
bower install
```

4) Update the project name in bower.json and package.json

5) ???

6) Profit


Using the Development Server
----------------------------
To start the local development server, just type `grunt serve` in the site root.


Deploying to Stage/Production
-----------------------------

In your deployment script, add the following commands:

```
npm install -g bower grunt-cli
bower install
bower update
npm install
grunt build
```

This will populate the "dist" directory with all of the final files that should be deployed to either the staging or production server. Once this is complete, use rsync or another copy mechanism to copy the contents of this directory to the target server.


Known Issues
------------
* The liverealod feature does not work with the PHP development server.
* TODO: Implement unit test tasks