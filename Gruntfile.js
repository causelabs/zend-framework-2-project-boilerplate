/*jslint node: true */
/*global module, require*/
"use strict";

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Configurable paths
    var config = {
        app: 'public',
        moduleDir: 'module',
        dist: 'dist',
        root: '.'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        config: config,

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            bower: {
                files: ['bower.json'],
                tasks: ['bowerInstall']
            },
            js: {
                files: ['<%= config.app %>/js/{,*/}*.js'],
                // tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            php: {
                files: ['<%= config.moduleDir %>/**.php', '<%= config.moduleDir %>/**/*.php'],
                options: {
                    livereload: true
                }
            },
            sass: {
                files: ['<%= config.app %>/css/{,*/}*.{scss,sass}'],
                tasks: ['sass:server', 'autoprefixer']
            },
            styles: {
                files: ['<%= config.app %>/css/{,*/}*.css'],
                tasks: ['newer:copy:styles', 'autoprefixer']
            },
            livereload: {
                options: {
                    livereload: 35729
                },
                files: [
                    '<%= config.app %>/{,*/}*.html',
                    '<%= config.app %>/{,*/}*.php',
                    '<%= config.app %>/{,*/}*.phtml',
                    '<%= config.app %>/data/{,*/}*',
                    '.tmp/css/{,*/}*.css',
                    '<%= config.app %>/img/{,*/}*'
                ]
            }
        },

        php: {
            dist: {
                options: {
                    port: 8001,
                    keepalive: true,
                    open: true,
                    router: 'local-router.php',
                    base: './public'
                }
            },
            watch: {}
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= config.dist %>/*',
                        '!<%= config.dist %>/.git*'
                    ]
                }]
            },
            sass: {
                files: [{
                    dot: true,
                    src: [
                        '<%= config.dist %>/public/css/*.scss'
                    ]
                }]
            },
            server: '.tmp'
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'Gruntfile.js',
                '<%= config.app %>/js/{,*/}*.js',
                '!<%= config.app %>/js/vendor/*',
                'test/spec/{,*/}*.js'
            ]
        },

        // Mocha testing framework configuration options
        mocha: {
            all: {
                options: {
                    run: true,
                    urls: ['http://<%= connect.test.options.hostname %>:<%= connect.test.options.port %>/index.html']
                }
            }
        },

        // Compiles Sass to CSS and generates necessary files if requested
        sass: {
            options: {
                includePaths: [
                    'bower_components'
                ]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.dist %>/public/css',
                    src: ['*.scss'],
                    dest: '<%= config.dist %>/public/css',
                    ext: '.css'
                }]
            },
            server: {
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>/css',
                    src: ['*.scss'],
                    dest: '.tmp/css',
                    ext: '.css'
                }]
            }
        },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 2 versions']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'dist/public/css/',
                    dest: 'dist/public/css/',
                    src: '{,*/}*.css'
                }]
            }
        },

        // The following *-min tasks produce minified files in the dist folder
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.dist %>/public/img',
                    src: '{,*/}*.{gif,jpeg,jpg,png}',
                    dest: '<%= config.dist %>/public/img'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.dist %>/public/img',
                    src: '{,*/}*.svg',
                    dest: '<%= config.dist %>/public/img'
                }]
            }
        },
        cssmin: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= config.dist %>/public/css/',
                        src: ['*.css', '!*.min.css'],
                        dest: '<%= config.dist %>/public/css/',
                        ext: '.css'
                    },
                    {
                        expand: true,
                        cwd: '<%= config.dist %>/public/bower_components/',
                        src: ['*.css', '!*.min.css'],
                        dest: '<%= config.dist %>/public/bower_components/',
                        ext: '.css'
                    }
                ]
            }
        },
        uglify: {
            options: {
                mangle: true,
                drop_console: true,
                report: 'min'
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= config.dist %>/public/js',
                        src: ['**/*.js', '!**/*.min.js'],
                        dest: '<%= config.dist %>/public/js'
                    },
                    {
                        expand: true,
                        cwd: '<%= config.dist %>/public/bower_components',
                        src: ['**/*.js', '!**/*.min.js'],
                        dest: '<%= config.dist %>/public/bower_components'
                    }
                ]
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '.',
                    dest: '<%= config.dist %>',
                    src: [
                        'config/**',
                        'data/**',
                        'module/**',
                        'public/**',
                        'vendor/**'
                    ]
                }]
            },
            bower: {
                files: [{
                    expand: true,
                    cwd: '.',
                    src: [
                        'bower_components/**/*.js',
                        'bower_components/**/*.map',
                        'bower_components/**/*.css',
                        'bower_components/**/*.png',
                        'bower_components/**/*.jpg',
                        'bower_components/**/*.gif',
                        '!bower_components/**/Gruntfile.js',
                        '!bower_components/**/src/*',
                        '!bower_components/**/*.src.js'
                    ],
                    dest: '<%= config.dist %>/public'
                }]
            },
            styles: {
                expand: true,
                dot: true,
                cwd: '<%= config.app %>/css',
                dest: '.tmp/css/',
                src: '{,*/}*.css'
            }
        },
        versioning: {
            options: {
                grepFiles: [
                    '<%= config.dist %>/module/**/*.phtml',
                ],
                keepOriginal: false
            },
            js: {
                src: [
                    '<%= config.dist %>/public/js/**.js'
                ]
            },
            css: {
                src: [
                    '<%= config.dist %>/public/css/**.css'
                ]
            },
            img: {
                src: [
                    '<%= config.dist %>/public/img/**.jpg',
                    '<%= config.dist %>/public/img/**.gif',
                    '<%= config.dist %>/public/img/**.svg',
                    '<%= config.dist %>/public/img/**.png',
                ]
            },
        },
        // Run some tasks in parallel to speed up build process
        concurrent: {
            server: [
                'sass:server',
                'copy:styles'
            ],
            test: [
                'copy:styles'
            ],
            dist: [
                'sass:dist',
                'imagemin',
                'svgmin'
            ],
            php: {
                tasks: ['php', 'watch'],
                options: {
                    logConcurrentOutput: true
                }
            }
        }
    });


    grunt.registerTask('serve', function () {

        grunt.task.run([
            'clean:server',
            'concurrent:server',
            'autoprefixer',
            'concurrent:php'
        ]);
    });

    grunt.registerTask('server', function (target) {
        grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
        grunt.task.run([target ? ('serve:' + target) : 'serve']);
    });

    grunt.registerTask('build', [
        'clean:dist',
        'copy:dist',
        'copy:bower',
        'concurrent:dist',
        'clean:sass',
        'uglify',
        'autoprefixer:dist',
        'cssmin',
        'versioning'
    ]);

    grunt.registerTask('default', [
        'newer:jshint',
        'test',
        'build'
    ]);

};
